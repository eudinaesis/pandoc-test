#!/bin/sh

pandoc -t revealjs -s --html-q-tags pnorthup-leistung-konzept.md -o public/index.html
copy -R images public
copy -R reveal.js public
pandoc -s pnorthup-leistung-konzept.md -o public/pnorthup-leistung-konzept.pdf --pdf-engine=xelatex
