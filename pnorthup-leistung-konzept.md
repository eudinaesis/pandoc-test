---
title: Konzept zur Leistungsfeststellung u. –beurteilung
author: Peter Northup
date: 18.1.2020
lang: de
margin: .15
width: 1280
height: 1024
theme: simple
revealjs-url: .
---

# Leistungsbeurteilung

## Sachliche Bezugsnorm

> Mit „Genügend“ sind Leistungen zu beurteilen, mit denen der Schüler die nach Maßgabe des Lehrplanes gestellten Anforderungen in der Erfassung und in der Anwendung des Lehrstoffes sowie in der Durchführung der Aufgaben in den wesentlichen Bereichen **überwiegend** erfüllt. (LBVO §15.4)

For bessere Noten:

- Hinausgehender Ausmaß über das Wesentliche
- Eigenständigkeit
- Anwendungen auf neuartige Aufgaben

## Zusammenfassung

![Arbeitsgruppe Prüfungskultur des Projekts IMST 2008, S. 88](images/pruefungskultur-notenraster.png)

## Konsequenzen für die Praxis

- Man muss "das Wesentliche" definieren
- Man muss Aufgaben und Bewertungssituationen so stellen, damit sowohl "wesentliche" Anforderungen als auch "erweiterte" Anforderungen verfügbar sind
- Man muss Möglichkeiten für Eigenständigkeit, "neuartige" Aufgaben geben

Darum:

Beurteilungsraster/Rubric (pro Aufgabe) → Kompetenzraster → Note

## Aber was ist "das Wesentliche"?

Empfehlung von Hinch et al. 2017:

Bildungs- und Lehraufgabe, Lehrstoff → wesentliche Bereiche:

## Wesentlicher Bereich 3: Bevölkerung und Gesellschaft

*Bevölkerung und Gesellschaft analysieren*

- Die heutige und die mögliche zukünftige Verteilung der Weltbevölkerung darstellen
- Dynamik der Weltbevölkerung analysieren
- Ursachen und Auswirkungen der räumlichen und sozialen Mobilität in verschiedenen Gesellschaften vergleichen

## Wesentlicher Bereich 5: Nutzungskonflikte an regionalen Beispielen

*Nutzungskonflikte an regionalen Beispielen reflektieren*

- Regionale Konflikte über die Verfügbarkeit von knappen Ressourcen (Boden, Wasser, Bodenschätze usw.) und dahinter stehende politische Interessen erklären
- Unterschiedliche Folgen von Naturereignissen aufgrund des sozialen und ökonomischen Gefüges beurteilen
- Tragfähigkeit der ‚Einen Welt‘ zukunftsorientiert reflektieren

## Entwurf zum Kompetenzraster

![Notenraster aus Arbeitsgruppe Prüfungskultur des Projekts IMST 2008, S. 88](images/gw-5k2s-kompetenzraster.png)

## Mögliche Probleme damit

- Kompensierbarkeit der unterschiedlichen "wesentlichen Bereichen"?
  * Nach Neuweg: nur **innerhalb** Kompetenzen, nicht dazwischen
  * Nachteil davon: hervorragende Leistung in allen anderen Bereichen → 5, wenn nur eine "wesentliche" Kompetenz fehlt 😱
- Was tun?
  * Mehrere Möglichkeiten pro Kompetenz
  * 2-Phase Arbeit
  * Frühwarnsystem

# Formen & Instrumente der Leistungsfestellung

## 100% Mitarbeit

- Wenige Prüfungsangst
- Größere Gestaltungsmöglichkeiten von Aufgaben
- Lebensnähere Aufgaben: "bessere" VMIn

## Nachteil: kein beurteilungsfreie Zeit?

Was tun dagegen?

- Nicht alles, das gemacht ist, zählt: manche Aufgaben sind nur "Vorbereitungen"
- "2-Phase Arbeit"
  * Alle Aufgabe mit Feedback zurückgegeben
  * Nur verbesserte Portfolio-Ausgaben zählen
  * Erste Entwürfe sind zum Lernen, nicht zum Beurteilen

# 5. Klasse, 2. Semester

## "Die soziale, ökonomisch und ökologisch begrenzte Welt"

- Vorher: 1. Semester
	* Gliederungsprinzipien der Erde nach unterschiedlichen Sichtweisen reflektieren
	* Geoökosysteme der Erde Analysieren
	* Die wirtschaftliche Bedürfnisse der Menschen bewerten
- **Bevölkerung und Gesellschaft diskutieren**
- **Nutzungskonflikte an regionalen Beispielen reflektieren**

# Unterrichtsziele & Kompetenzen

## Bevölkerung und Gesellschaft diskutieren

- Die heutige und die mögliche zukünftige Verteilung der Weltbevölkerung darstellen
- Dynamik der Weltbevölkerung analysieren
- Ursachen und Auswirkungen der räumlichen und sozialen Mobilität in verschiedenen Gesellschaften diskutieren

## Nutzungskonflikte an regionalen Beispielen reflektieren

- Regionale Konflikte über die Verfügbarkeit von knappen Ressourcen (Boden, Wasser, Bodenschätze, usw.) und dahinter stehende politische Interessen erklären
- Unterschiedliche Folgen von Naturereignissen aufgrund des sozialen und ökonomischen Gefüges beurteilen
- Tragfähigkeit der Einen Welt zukunftsorientiert reflektieren

# Konkretes Beispiel

## "Ursachen und Auswirkungen der räumlichen und sozialen Mobilität in verschiedenen Gesellschaften vergleichen"

- Frontalunterricht + Aufgaben & Stationenbetrieb zur "Vorbereitung"
- Viele davon kann als Basis für ausgewertete Ausgabe benutzt werden
  * z.B. Vergleich von Arbeitsmarkt-Auswirkungen von Migration zwischen Saudi Arabien und Österreich
- Nach Feedback, kann verbessert werden
- Bewertet nach Raster/Rubric, damit es transparent in das Kompetenzraster fließt

# Offene Fragen

## Wo/wie fließen die anderen Kompetenzen herein?

z.B. soziale-kommunicative, persönlich, methodisch-strategisch ...

Vielleicht: als Kompetenzen, die nur "erweiterte Anforderungen" stellen

# Quellen

## Literatur

Arbeitsgruppe Prüfungskultur des Projekts IMST ed. 2008. Prüfungskultur: Leistung und Bewertung (in) der Schule. Klagenfurt: Inst. für Unterrichts- und Schulentwicklung.

Hinsch, S., H. Pichler, and T. Jekel. 2017. Wesentliche Bereiche des Lehrplans Geographie und Wirtschaftskunde als Beurteilungsgrundlage. GW-Unterricht 1:80–84.

Neuweg, G. H. 2019. Kompetenzorientierte Leistungsbeurteilung: pädagogische und rechtliche Hilfestellungen für die Schulpraxis. Linz: Trauner Verlag + Buchservice GmbH.

Stern, T. 2010. Förderliche Leistungsbewertung 2nd ed. Wien: Österreichisches Zentrum für Persönlichkeitsbildung und soziales Lernen.
